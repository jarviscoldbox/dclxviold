# We're using LinuxMint
FROM dasbastard/slim-buster:latest

#
# Clone repo and prepare working directory
#
RUN git clone -b master https://gitlab.com/jarviscoldbox/DCLXVI /home/dclxvi/
RUN mkdir /home/dclxvi/bin/
WORKDIR /home/dclxvi/

CMD ["python3","-m","userbot"]
